<?php
/* @var $this NoticiasController */
/* @var $data Noticias */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idNoticia')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idNoticia), array('view', 'id'=>$data->idNoticia)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo')); ?>:</b>
	<?php echo CHtml::encode($data->titulo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contenido')); ?>:</b>
	<?php echo CHtml::encode($data->contenido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idProyecto')); ?>:</b>
	<?php echo CHtml::encode($data->idProyecto); ?>
	<br />


</div>