<?php
/* @var $this ProyectosController */
/* @var $model Proyectos */

$this->breadcrumbs=array(
	'Proyectoses'=>array('index'),
	$model->idProyecto=>array('view','id'=>$model->idProyecto),
	'Update',
);

$this->menu=array(
	array('label'=>'List Proyectos', 'url'=>array('index')),
	array('label'=>'Create Proyectos', 'url'=>array('create')),
	array('label'=>'View Proyectos', 'url'=>array('view', 'id'=>$model->idProyecto)),
	array('label'=>'Manage Proyectos', 'url'=>array('admin')),
);
?>

<h1>Update Proyectos <?php echo $model->idProyecto; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>