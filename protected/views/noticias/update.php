<?php
/* @var $this NoticiasController */
/* @var $model Noticias */

$this->breadcrumbs=array(
	'Noticiases'=>array('index'),
	$model->idNoticia=>array('view','id'=>$model->idNoticia),
	'Update',
);

$this->menu=array(
	array('label'=>'List Noticias', 'url'=>array('index')),
	array('label'=>'Create Noticias', 'url'=>array('create')),
	array('label'=>'View Noticias', 'url'=>array('view', 'id'=>$model->idNoticia)),
	array('label'=>'Manage Noticias', 'url'=>array('admin')),
);
?>

<h1>Update Noticias <?php echo $model->idNoticia; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>