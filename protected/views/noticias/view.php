<?php
/* @var $this NoticiasController */
/* @var $model Noticias */

$this->breadcrumbs=array(
	'Noticiases'=>array('index'),
	$model->idNoticia,
);

$this->menu=array(
	array('label'=>'List Noticias', 'url'=>array('index')),
	array('label'=>'Create Noticias', 'url'=>array('create')),
	array('label'=>'Update Noticias', 'url'=>array('update', 'id'=>$model->idNoticia)),
	array('label'=>'Delete Noticias', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idNoticia),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Noticias', 'url'=>array('admin')),
);
?>

<h1>View Noticias #<?php echo $model->idNoticia; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idNoticia',
		'titulo',
		'contenido',
		'idProyecto',
	),
)); ?>
